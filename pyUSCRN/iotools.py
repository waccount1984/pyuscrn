#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, cf_units, urllib, warnings, time

warnings.filterwarnings("ignore")
import pandas as pd
import numpy as np
#import geopandas as gpd
import xarray as xr
from netCDF4 import date2num
import pyUSCRN as pu

pd.set_option('display.max_columns', None)


# ===============================================================================

def convert_to_ILAMB_benchmark(header, data,
                                     out='test.nc',
                                     varname='tas',
                                     varunit=None,
                                     timeunit=None,
                                     timecalendar=None,
                                     scale = None):
                                     
    '''
    
    This function is to convert and save the harvested **daily** and **monthly** data to the file ILAMB required.
    
    :param header: header of stations, including WBANNO, LATITUDE, and LONGITUDE.
    :type header: pandas.DataFrame
     
    :param data: data the given variable for all sites, it has an index of datetime-style.
    :type data: pandas.DataFrame
    
    :param out: output file, default to 'test.nc'
    :type out: str
    
    :param varname: variable name to be wrote in the output file, default to 'tas'. 
    :type varname: str    

    :param varunit: variable unit to be wrote in the output file, default to None, i.e., keep raw unit.
    :type varunit: str, cf-units standard  

    :param timeunit: time unit to be wrote in the output file, default to 'days since 1850-01-01 00:00:00'.
    :type timeunit: str, cftime standard
    
    :param timecalendar: time calendar to be wrote in the output file, default to 'proleptic_gregorian'.
    :type timecalendar: str, cftime standard    

    :param scale: time scale, could be 'daily', 'monthly', 'hourly', or 'subhourly', default to 'Daily', default to None, which means detecting from the data.
    :type scale: str 
        
    :Example:
    
    If we want to combine daily air temperature data across all sites in Colorado, then write to nc file.
    
    >>> import pyUSCRN as pu
    >>> stn_list = pu.filter_stations(states=['CO'])
    >>> pu.plot_map_selected_stations()
    >>> stn_header, data = pu.read_merge_sites_for_single_variable(ID = stn_list.ID, VARS = ['T_DAILY_AVG'], scale = 'daily')
    >>> data.plot()
    >>> pu.convert_to_ILAMB_benchmark(stn_header, data, 'tas.nc')
    
    '''
    
    if scale is None:
        
        assert data.index.freqstr in ['M','D']
        
        if data.index.freqstr == 'M':
            
            scale = 'monthly'
        
        if data.index.freqstr == 'D':
            
            scale = 'daily'
    
    assert scale.lower() in ['daily', 'monthly', 'hourly', 'subhourly']
    
    if scale.lower() == 'daily':
        
        if timeunit is None:
            
            timeunit = 'days since 1850-01-01 00:00:00'
        
        if timecalendar is None:
        
            timecalendar='noleap'
        
        convert_to_ILAMB_benchmark_daily(header, data,
                                     out=out,
                                     varname=varname,
                                     varunit=varunit,
                                     timeunit=timeunit,
                                     timecalendar=timecalendar)

    if scale.lower() == 'monthly':
        
        if timeunit is None:
            timeunit='days since 1850-01-01 00:00:00'
            
        if timecalendar is None:   
            timecalendar='proleptic_gregorian'
        
        convert_to_ILAMB_benchmark_monthly(header, data,
                                     out=out,
                                     varname=varname,
                                     varunit=varunit,
                                     timeunit=timeunit,
                                     timecalendar=timecalendar)
    

def convert_to_ILAMB_benchmark_daily(header, data,
                                     out='test.nc',
                                     varname='tas',
                                     varunit=None,
                                     timeunit='days since 1850-01-01 00:00:00',
                                     timecalendar='noleap'):

    '''
    
    This function is to convert and save the harvested **daily** data to the file ILAMB required.
    
    :param header: header of stations, including WBANNO, LATITUDE, and LONGITUDE.
    :type header: pandas.DataFrame
     
    :param data: data the given variable for all sites, it has an index of datetime-style.
    :type data: pandas.DataFrame
    
    :param out: output file, default to 'test.nc'
    :type out: str
    
    :param varname: variable name to be wrote in the output file, default to 'tas'. 
    :type varname: str    

    :param varunit: variable unit to be wrote in the output file, default to None, i.e., keep raw unit.
    :type varunit: str, cf-units standard  

    :param timeunit: time unit to be wrote in the output file, default to 'days since 1850-01-01 00:00:00'.
    :type timeunit: str, cftime standard
    
    :param timecalendar: time calendar to be wrote in the output file, default to 'proleptic_gregorian'.
    :type timecalendar: str, cftime standard    
        
    :Example:
    
    If we want to combine daily air temperature data across all sites in Colorado, then write to nc file.
    
    >>> import pyUSCRN as pu
    >>> stn_list = pu.filter_stations(states=['CO'])
    >>> pu.plot_map_selected_stations()
    >>> stn_header, data = pu.read_merge_sites_for_single_variable(ID = stn_list.ID, VARS = ['T_DAILY_AVG'], scale = 'daily')
    >>> data.plot()
    >>> pu.convert_to_ILAMB_benchmark_daily(stn_header, data, 'tas.nc')
    
    '''
    
    assert type(data.index).__name__.lower().find('date') >= 0
    assert data.shape[1] == header.shape[1]

    time = data.index
    
    if varunit is None:
                
        assert len(np.unique(list(header.loc['Unit']))) == 1
        
        varunit = np.unique(list(header.loc['Unit']))
        
    else:
        
        old_unit = np.unique(list(header.loc['Unit']))
        assert cf_units.Unit(old_unit[0])
        
        old_unit = cf_units.Unit(old_unit[0])
        new_unit  = cf_units.Unit(varunit)
        
        new_data = old_unit.convert(data.values, new_unit)
        
        data.set_value(index= data.index, col=data.columns, value=new_data)

    assert type(time.freq).__name__ == 'Day'

    if timecalendar in ['365_day', 'noleap']:
        warnings.warn('You are chosing noleap calendar. Feb-29 will be all removed.')

        idx = np.where(((time.month == 2) & (time.day == 29)) == False)[0]

        time = time[idx]
        data = data.iloc[idx, :]

    time_end = time + pd.DateOffset(days=1)
    time_onset = time.copy()

    idx_true = np.where(((time_end.month == 2) & (time_end.day == 29)) == True)[0]

    if timecalendar in ['365_day', 'noleap']:
        for idsfds in idx_true:
            time_end.values[idsfds] = time_end[idsfds].replace(year=time_end[idsfds].year,
                                                               month=time_end[idsfds].month + 1, day=1)

    time = date2num(time.to_pydatetime(),
                    timeunit,
                    calendar=timecalendar)

    time = xr.DataArray(time, coords=[time], dims=['time'])
    time.attrs['units'] = timeunit
    time.attrs['calendar'] = timecalendar
    time.attrs['bounds'] = 'time_bnds'

    time_end = date2num(time_end.to_pydatetime(),
                        timeunit,
                        calendar=timecalendar)

    time_onset = date2num(time_onset.to_pydatetime(),
                          timeunit,
                          calendar=timecalendar)

    stns = header.loc['WBANNO'].values.astype('float')
    lats = header.loc['LATITUDE'].values.astype('float')
    lons = header.loc['LONGITUDE'].values.astype('float')

    foo = xr.DataArray(data.values.astype('float'), coords=[time, stns], dims=['time', 'data'])
    lat = xr.DataArray(lats, coords=[stns], dims=['data'])
    lon = xr.DataArray(lons, coords=[stns], dims=['data'])

    nv = [0, 1]

    time_bnds = xr.DataArray(np.c_[time_onset, time_end], coords=[time, nv], dims=['time', 'nv'])

    foo.name = 'foo'
    lat.name = 'lat'
    lon.name = 'lon'

    foo.attrs['units'] = varunit
    lat.attrs['units'] = 'degrees_north'
    lon.attrs['units'] = 'degrees_east'
    lat.attrs['standard_name'] = "latitude"
    lon.attrs['standard_name'] = "longitude"

    fid = xr.Dataset({varname: foo, 'lat': lat, 'lon': lon, 'time': time, 'time_bnds': time_bnds})

    fid.to_netcdf(out)


def convert_to_ILAMB_benchmark_monthly(header, data,
                                       out='test.nc',
                                       varname='tas',
                                       varunit=None,
                                       timeunit='days since 1850-01-01 00:00:00',
                                       timecalendar='proleptic_gregorian'):
    
    '''
    
    This function is to convert and save the harvested **monthly** data to the file ILAMB required.
    
    :param header: header of stations, including WBANNO, LATITUDE, and LONGITUDE.
    :type header: pandas.DataFrame
     
    :param data: data the given variable for all sites, it has an index of datetime-style.
    :type data: pandas.DataFrame
    
    :param out: output file, default to 'test.nc'
    :type out: str
    
    :param varname: variable name to be wrote in the output file, default to 'tas'. 
    :type varname: str    

    :param varunit: variable unit to be wrote in the output file, default to None, i.e., keep raw unit.
    :type varunit: str, cf-units standard  

    :param timeunit: time unit to be wrote in the output file, default to 'days since 1850-01-01 00:00:00'.
    :type timeunit: str, cftime standard
    
    :param timecalendar: time calendar to be wrote in the output file, default to 'proleptic_gregorian'.
    :type timecalendar: str, cftime standard    
        
    :Example:
    
    If we want to combine monthly air temperature data across all sites in Colorado, then write to nc file.
    
    >>> import pyUSCRN as pu
    >>> stn_list = pu.filter_stations(states=['CO'])
    >>> pu.plot_map_selected_stations()
    >>> stn_header, data = pu.read_merge_sites_for_single_variable(ID = stn_list.ID, VARS = ['T_MONTHLY_AVG'], scale = 'monthly')
    >>> data.plot()
    >>> pu.convert_to_ILAMB_benchmark_monthly(stn_header, data, 'tas.nc')
    
    '''
    
    assert type(data.index).__name__.lower().find('date') >= 0

    assert data.shape[1] == header.shape[1]

    time = data.index
    assert type(time.freq).__name__ == 'MonthEnd'
    
    if varunit is None:
                
        assert len(np.unique(list(header.loc['Unit']))) == 1
        
        varunit = np.unique(list(header.loc['Unit']))
        
    else:
        
        old_unit = np.unique(list(header.loc['Unit']))
        assert cf_units.Unit(old_unit[0])
        
        old_unit = cf_units.Unit(old_unit[0])
        new_unit  = cf_units.Unit(varunit)
        
        new_data = old_unit.convert(data.values, new_unit)
        
        data.set_value(index= data.index, col=data.columns, value=new_data)
        
        
    time_end = time + pd.DateOffset(days=1)
    time_onset = time + pd.DateOffset(days=1) + pd.DateOffset(months=-1)

    time = date2num(time.to_pydatetime(),
                    timeunit,
                    calendar=timecalendar)

    time_end = date2num(time_end.to_pydatetime(),
                        timeunit,
                        calendar=timecalendar)

    time_onset = date2num(time_onset.to_pydatetime(),
                          timeunit,
                          calendar=timecalendar)

    time = xr.DataArray(time, coords=[time], dims=['time'])
    time.attrs['units'] = timeunit
    time.attrs['calendar'] = timecalendar
    time.attrs['bounds'] = 'time_bnds'

    stns = header.loc['WBANNO'].values.astype('float')
    lats = header.loc['LATITUDE'].values.astype('float')
    lons = header.loc['LONGITUDE'].values.astype('float')

    nv = [0, 1]

    foo = xr.DataArray(data.values.astype('float'), coords=[time, stns], dims=['time', 'data'])
    lat = xr.DataArray(lats, coords=[stns], dims=['data'])
    lon = xr.DataArray(lons, coords=[stns], dims=['data'])

    time_bnds = xr.DataArray(np.c_[time_onset, time_end], coords=[time, nv], dims=['time', 'nv'])

    foo.name = 'foo'
    lat.name = 'lat'
    lon.name = 'lon'

    foo.attrs['units'] = varunit
    lat.attrs['units'] = 'degrees_north'
    lon.attrs['units'] = 'degrees_east'
    lat.attrs['standard_name'] = "latitude"
    lon.attrs['standard_name'] = "longitude"

    fid = xr.Dataset({varname: foo, 'lat': lat, 'lon': lon, 'time': time, 'time_bnds': time_bnds})

    fid.to_netcdf(out)

# ==============================================================================

def list_available_variables(scale='daily'):
    
    '''
    This function is to list available variables for different time scales.
    
    :param scale: time scale, could be 'daily', 'monthly', 'hourly', or 'subhourly', default to 'Daily'.
    :type scale: str    
    
    :Example:
    
    >>> import pyUSCRN as pu
    >>> pu.list_available_variables()
    
    '''
    
    if scale.lower() == 'subhourly':

        n = len(pu.header_subhourly)
        print('----------------------------------------')
        print('There are ', n, ' variables [' + scale.upper() + ']')
        print('----------------------------------------')
        print('{:25} | {:10}'.format('Variable', 'Unit'))
        print('----------------------------------------')
        for i in np.arange(n):
            print('{:25} | {:10}'.format(list(pu.header_subhourly.items())[i][0],
                                         list(pu.header_subhourly.items())[i][1]))

        print('----------------------------------------')

    if scale.lower() == 'daily':

        n = len(pu.header_daily)
        print('----------------------------------------')
        print('There are ', n, ' variables [' + scale.upper() + ']')
        print('----------------------------------------')
        print('{:25} | {:10}'.format('Variable', 'Unit'))
        print('----------------------------------------')
        for i in np.arange(n):
            print('{:25} | {:10}'.format(list(pu.header_daily.items())[i][0], list(pu.header_daily.items())[i][1]))

        print('----------------------------------------')

    if scale.lower() == 'hourly':

        n = len(pu.header_hourly)
        print('----------------------------------------')
        print('There are ', n, ' variables [' + scale.upper() + ']')
        print('----------------------------------------')
        print('{:25} | {:10}'.format('Variable', 'Unit'))
        print('----------------------------------------')
        for i in np.arange(n):
            print('{:25} | {:10}'.format(list(pu.header_hourly.items())[i][0], list(pu.header_hourly.items())[i][1]))

        print('----------------------------------------')

    if scale.lower() == 'monthly':

        n = len(pu.header_monthly)
        print('----------------------------------------')
        print('There are ', n, ' variables [' + scale.upper() + ']')
        print('----------------------------------------')
        print('{:25} | {:10}'.format('Variable', 'Unit'))
        print('----------------------------------------')
        for i in np.arange(n):
            print('{:25} | {:10}'.format(list(pu.header_monthly.items())[i][0], list(pu.header_monthly.items())[i][1]))

        print('----------------------------------------')

    # ==============================================================================


def read_merge_sites_for_single_variable(ID=None,
                                         DATES=['2016-01-01 00:00:00', '2016-12-31 23:59:59'],
                                         VARS=None,
                                         scale='Daily',
                                         mindata=1,
                                         quiet=False):
    '''

    This function is to read multiple sites data and merge them by the given variable.

    :param ID: station IDs to be read.
    :type ID: list

    :param DATES: date range, the first element is onset, last one is endding. It defaults to ['2016-01-01 00:00:00', '2016-12-31 23:59:59'].
    :type DATES: list, lengh = 2

    :param VARS: variable list to be extracted, default to None. It has to be sepecified.
    :type VARS: str, list, **Mandatory**

    :param scale: time scale, could be 'daily', 'monthly', 'hourly', or 'subhourly', default to 'Daily'.
    :type scale: str

    :param mindata: minimum number of data for sites, default to 1.
    :type mindata: int

    :param quiet: block all screen prints, default to False.
    :param quiet: bool

    :return: 
       
      return1: header of stations, including WBANNO, LATITUDE, and LONGITUDE.
      
      return2: data the given variable for all sites, it has an index of datetime-style.
         
    :rtype:
    
      return1: pandas.DataFrame

      return2: pandas.DataFrame

    :Example:

    If we want to combine monthly air temperature data across all sites in Colorado
    
    >>> import pyUSCRN as pu
    >>> stn_list = pu.filter_stations(states=['CO'])
    >>> pu.plot_map_selected_stations()
    >>> stn_header, data = pu.read_merge_sites_for_single_variable(ID = stn_list.ID, VARS = ['T_MONTHLY_AVG'], scale = 'monthly')
    >>> data.plot()


    '''

    assert VARS is not None
    assert type(VARS).__name__ in ('list')
    assert len(VARS) == 1
    assert len(DATES) == 2
    assert scale.lower() in ['daily', 'monthly', 'hourly', 'subhourly']

    cont = -1
    
    time_start = time.time()

    if ID is not None:

        for i, stn in enumerate(ID):

            data = pu.read_single_site([stn],
                                       scale=scale,
                                       VARS=VARS,
                                       DATES=DATES)

            if data is not None:

                cont += 1

                data.rename(columns={VARS[0]: stn}, inplace=True)
                data2 = data.loc[data.index[0], list(np.setdiff1d(data.columns, stn))]
                data3 = data[stn]
                data = data2.append(data3)
                data.name = stn

                data2.name = stn

                if cont == 0:
                    stn_header = data2.to_frame()
                    data_all = data3.to_frame()
                else:
                    data_all[stn] = data3.values.astype('float')
                    stn_header[stn] = data2

            if quiet == False:

                if i == 0:

                    print('merging ({:d}) stations'.format(len(ID)), end='')

                else:

                    if (i == len(ID) - 1):

                        print('DONE, '+str(np.round(time.time()-time_start, 3))+' s')
                        
                    else:

                        print('.', end='')

        #        data_all
        valid_num = data_all.count(axis=0)

        idx = np.where(valid_num >= mindata)[0]

        data_all = data_all.iloc[:, idx]

        stn_header = stn_header.iloc[:, idx]
        
        
        if scale.lower() == 'daily':
        
            stn_header.loc['Unit'] = pu.header_daily[VARS[0]]

        if scale.lower() == 'monthly':
                    
            stn_header.loc['Unit'] = pu.header_monthly[VARS[0]]
        
        if scale.lower() == 'hourly':
                    
            stn_header.loc['Unit'] = pu.header_hourly[VARS[0]]

        if scale.lower() == 'subhourly':
                    
            stn_header.loc['Unit'] = pu.header_subhourly[VARS[0]]
            
        stn_header.loc['Variable'] = VARS[0]

    return stn_header, data_all


# ==============================================================================

def read_single_site(ID=None,
                     DATES=['2016-01-01 00:00:00', '2016-12-31 23:59:59'],
                     VARS=None,
                     scale='Daily'):
    '''
    
    This function is to read data at single site.
        
    To check available variables, for example in monthly data, please:
    
    >>> import pyUSCRN as pu
    >>> pu.list_available_variables(scale='monthly')
    
    :param ID: single station ID to be read.
    :type ID: list
    
    :param DATES: date range, the first element is onset, last one is endding. It defaults to ['2016-01-01 00:00:00', '2016-12-31 23:59:59'].
    :type DATES: list, lengh = 2
    
    :param VARS: variable list to be extracted, default to None. It has to be sepecified. 
    :type VARS: str, list, **Mandatory**
    
    :param scale: time scale, could be 'daily', 'monthly', 'hourly', or 'subhourly', default to 'Daily'.
    :type scale: str
    
    :return: selected variables for given site, it has an index of datetime-style.
    :rtype: pandas.DataFrame
    
    :Example:
    
    If we want to read data at Barrow, where ID is 'AK_Utqiagvik_formerly_Barrow_4_ENE'.
    
    >>> import pyUSCRN as pu
    >>> ﻿data = pu.read_single_site(ID=['AK_Utqiagvik_formerly_Barrow_4_ENE'], 
                                    scale = 'monthly', 
                                    VARS=['T_MONTHLY_AVG'])
    
    Then you can quick plot the processed data. For example, to show air temperature:
    
    >>> data.T_MONTHLY_AVG.plot()
                 
    '''

    assert scale.lower() in ['daily', 'monthly', 'hourly', 'subhourly']

    if scale.lower() == 'daily':

        if (VARS is None):

            VARS = list(pu.header_daily)

        else:

            assert type(VARS).__name__ in ('list')

        data = read_single_site_daily(ID=ID, DATES=[DATES[0][0:10], DATES[1][0:10]],
                                      VARS=VARS)

    if scale.lower() == 'monthly':

        if (VARS is None):

            VARS = list(pu.header_monthly)

        else:

            assert type(VARS).__name__ in ('list')

        data = read_single_site_monthly(ID=ID, DATES=[DATES[0][0:7], DATES[1][0:7]],
                                        VARS=VARS)

    if scale.lower() == 'hourly':

        if (VARS is None):

            VARS = list(pu.header_hourly)

        else:

            assert type(VARS).__name__ in ('list')

        data = read_single_site_hourly(ID=ID, DATES=[DATES[0], DATES[1]],
                                       VARS=VARS)

    if scale.lower() == 'subhourly':

        if (VARS is None):

            VARS = list(pu.header_subhourly)

        else:

            assert type(VARS).__name__ in ('list')

        data = read_single_site_subhourly(ID=ID, DATES=[DATES[0], DATES[1]],
                                          VARS=VARS)

    return data


def read_single_site_monthly(ID=None, DATES=['2000-01', '2016-12'],
                             VARS=['T_MONTHLY_MAX',
                                   'T_MONTHLY_MIN',
                                   'T_MONTHLY_AVG',
                                   'P_MONTHLY_CALC',
                                   'SOLRAD_MONTHLY_AVG',
                                   'SUR_TEMP_MONTHLY_MAX',
                                   'SUR_TEMP_MONTHLY_MIN',
                                   'SUR_TEMP_MONTHLY_AVG'],
                              info_col = ['WBANNO', 'PRECISE_LATITUDE', 'PRECISE_LONGITUDE']):
    '''
    
    This function is to read **monthly** data at single site. The daily time series is an interval of 1-month.
    
    To check available variables in monthly data, please:
    
    >>> import pyUSCRN as pu
    >>> pu.list_available_variables(scale='monthly')
    
    :param ID: single station ID to be read.
    :type ID: list
    
    :param DATES: date range, the first element is onset, last one is endding. It defaults to ['2000-01','2016-12'].
    :type DATES: list, lengh = 2
    
    :param VARS: variable list to be extracted, default to ['T_MONTHLY_MAX', 'T_MONTHLY_MIN', 'T_MONTHLY_AVG', 'P_MONTHLY_CALC', 'SOLRAD_MONTHLY_AVG', 'SUR_TEMP_MONTHLY_MAX', 'SUR_TEMP_MONTHLY_MIN', 'SUR_TEMP_MONTHLY_AVG'].
    :type VARS: str, list
    
    :return: selected variables for given site, it has an index of datetime-style.
    :rtype: pandas.DataFrame
    
    :Example:
    
    If we want to read data at Barrow, where ID is 'AK_Utqiagvik_formerly_Barrow_4_ENE'.
    
    >>> import pyUSCRN as pu
    >>> data = pu.read_single_site_monthly(ID=['AK_Utqiagvik_formerly_Barrow_4_ENE'])
    
    Then you can quick plot the processed data. For example, to show air temperature:
    
    >>> data.T_MONTHLY_AVG.plot()
    
    :NOTE:
    
    Solar radiation ['SOLRAD_MONTHLY_AVG'] has been converted from 'MJ m-2' to 'Watt m-2'.
             
    '''

    if ID is not None:
    
#        ID = list([ID])

        assert type(ID).__name__ in ('list')
        assert len(ID) == 1

        VARS = list(VARS)

        pu.check_site_files(ID=ID, scale='Monthly')

        lst = pu.file_dict[pu.file_dict.ID.isin(ID) & pu.file_dict.Source.isin(['Monthly'])]

        assert lst.shape[0] > 0

        lst.index = np.arange(lst.shape[0])

        for i, nsm in enumerate(lst.Basename):

            file = os.path.join(pu.dump_directory, nsm)

            data0 = pd.read_fwf(file, na_values=['-99.000', '-9999.0', '-99999', '99999', '-9999.00'], header=None,
                                widths=pu.width_monthly)
            data0.columns = list(pu.header_monthly.keys())

            if i == 0:
                data = data0.copy()
            else:
                data = data.append(data0)

        DATES = pd.date_range(start=DATES[0] + '-01', end=DATES[1] + '-31', freq='M')  # - pd.DateOffset(days=15)

        columns_out = data.columns

        data_out = pd.DataFrame(columns=columns_out)
        data_out.LST_YRMO = DATES.year * 100 + DATES.month

        data_out.PRECISE_LONGITUDE = data.PRECISE_LONGITUDE.values[0]
        data_out.PRECISE_LATITUDE = data.PRECISE_LATITUDE.values[0]
        data_out.WBANNO = data.WBANNO.values[0]

        idx0, ia0, ib0 = np.intersect1d(data_out.LST_YRMO, data.LST_YRMO, return_indices=True)

        

        var_raw = np.setdiff1d(columns_out, info_col)

        for ii in var_raw:

            if ii.lower().find('solrad') >= 0:

                data_out[ii].values[ia0] = data[ii].values[ib0] / 86400 * 1E6
            else:
                data_out[ii].values[ia0] = data[ii].values[ib0]

        data_out = data_out.set_index(DATES)

        test = info_col + VARS

        data_out = data_out[test].astype('float')

        data_out = data_out.rename(columns={'LST_YRMO': 'LST_DATE',
                                            'PRECISE_LATITUDE': 'LATITUDE',
                                            'PRECISE_LONGITUDE': 'LONGITUDE'})

    return data_out


def read_single_site_daily(ID=None, DATES=['2000-01-01', '2016-12-31'],
                           VARS=['T_DAILY_AVG',
                                 'P_DAILY_CALC',
                                 'SOLARAD_DAILY',
                                 'RH_DAILY_AVG'],
                           info_col = ['WBANNO', 'LONGITUDE', 'LATITUDE']):
    '''
    
    This function is to read **daily** data at single site. The daily time series is an interval of 1-day.
    
    To check available variables in daily data, please:
    
    >>> import pyUSCRN as pu
    >>> pu.list_available_variables(scale='daily')
    
    :param ID: single station ID to be read.
    :type ID: list
    
    :param DATES: date range, the first element is onset, last one is endding. It defaults to ['2000-01-01','2016-12-31'].
    :type DATES: list, lengh = 2
    
    :param VARS: variable list to be extracted, default to ['T_DAILY_AVG', 'P_DAILY_CALC', 'SOLARAD_DAILY', 'RH_DAILY_AVG'].
    :type VARS: str, list
    
    :return: selected variables for given site, it has an index of datetime-style.
    :rtype: pandas.DataFrame
    
    :Example:
    
    If we want to read data at Barrow, where ID is 'AK_Utqiagvik_formerly_Barrow_4_ENE'.
    
    >>> import pyUSCRN as pu
    >>> data = pu.read_single_site_daily(ID=['AK_Utqiagvik_formerly_Barrow_4_ENE'])
    
    Then you can quick plot the processed data. For example, to show air temperature:
    
    >>> data.T_DAILY_AVG.plot()
    
    :NOTE:
    
    Solar radiation ['SOLARAD_DAILY'] has been converted from 'MJ m-2' to 'Watt m-2'.       
       
    '''

    if ID is not None:
        
#        ID = list([ID])

        assert type(ID).__name__ in ('list')
        assert len(ID) == 1
        
        VARS = list(VARS)

        pu.check_site_files(ID=ID, scale='Daily')

        lst = pu.file_dict[pu.file_dict.ID.isin(ID) &
                           pu.file_dict.Source.isin(['Daily'])]

        assert lst.shape[0] > 0

        lst.index = np.arange(lst.shape[0])

        for i, nsm in enumerate(lst.Basename):

            file = os.path.join(pu.dump_directory, nsm)

            data0 = pd.read_fwf(file,
                                na_values=['-99.000', '-9999.0', '-99999', '-9999.00', '99999'],
                                header=None, widths=pu.width_daily)

            data0.columns = list(pu.header_daily.keys())

            if i == 0:
                data = data0.copy()
            else:
                data = data.append(data0)

        DATES = pd.date_range(start=DATES[0], end=DATES[1], freq='D')

        columns_out = data.columns

        data_out = pd.DataFrame(columns=columns_out)
        data_out.LST_DATE = DATES.year * 10000 + DATES.month * 100 + DATES.day

        data_out.LONGITUDE = data.LONGITUDE.values[0]
        data_out.LATITUDE = data.LATITUDE.values[0]
        data_out.WBANNO = data.WBANNO.values[0]

        idx0, ia0, ib0 = np.intersect1d(data_out.LST_DATE,
                                        data.LST_DATE,
                                        return_indices=True)

        

        var_raw = np.setdiff1d(columns_out, info_col)

        for ii in var_raw:

            if ii.lower().find('solarad') >= 0:

                data_out[ii].values[ia0] = data[ii].values[ib0] / 86400 * 1E6

            else:

                data_out[ii].values[ia0] = data[ii].values[ib0]

        test = info_col + VARS

        data_out = data_out[test].astype('float')

        data_out = data_out.set_index(DATES)

    return data_out


def read_single_site_hourly(ID=None,
                            DATES=['2016-01-01 00:00:00',
                                   '2016-12-31 23:59:59'],
                            VARS=['T_HR_AVG', 'P_CALC', 'SOLARAD', 'RH_HR_AVG'],
                            info_col = ['WBANNO', 'LONGITUDE', 'LATITUDE']):
    '''
    
    This function is to read **hourly** data at single site. The hourly time series is an interval of 1-hour.
    
    To check available variables in hourly data, please:
    
    >>> import pyUSCRN as pu
    >>> pu.list_available_variables(scale='hourly')
    
    :param ID: single station ID to be read.
    :type ID: list
    
    :param DATES: date range, the first element is onset, last one is endding. It defaults to ['2016-01-01 00:00:00', '2016-12-31 23:59:59']
    :type DATES: list, lengh = 2
    
    :param VARS: variable list to be extracted, default to ['T_HR_AVG', 'P_CALC', 'SOLARAD', 'RH_HR_AVG'].
    :type VARS: str, list
    
    :return: selected variables for given site, it has an index of datetime-style.
    :rtype: pandas.DataFrame
    
    :Example:
    
    If we want to read data at Barrow, where ID is 'AK_Utqiagvik_formerly_Barrow_4_ENE'.
    
    >>> import pyUSCRN as pu
    >>> data = pu.read_single_site_hourly(ID=['AK_Utqiagvik_formerly_Barrow_4_ENE'])
    
    Then you can quick plot the processed data. For example, to show air temperature:
    
    >>> data.T_HR_AVG.plot()
               
    '''

    if ID is not None:

#        ID = list([ID])
        
        assert type(ID).__name__ in ('list')
        assert len(ID) == 1

        VARS = list(VARS)

        year_range_limit = np.arange(int(DATES[0][0:4]), int(DATES[1][0:4]) + 2)

        #        if int(DATES[1][0:4])==int(DATES[0][0:4]):
        #            year_range_limit = np.arange(int(DATES[0][0:4]),int(DATES[1][0:4])+1)
        #        else:
        #            year_range_limit = np.arange(int(DATES[0][0:4]),int(DATES[1][0:4])+1)

        pu.check_site_files(ID=ID, scale='Hourly')

        lst = pu.file_dict[pu.file_dict.ID.isin(ID) &
                           pu.file_dict.Source.isin(['Hourly']) &
                           pu.file_dict.YEAR.isin(list(year_range_limit))]

        try:

            assert lst.shape[0] > 0

            lst.index = np.arange(lst.shape[0])

            for i, nsm in enumerate(lst.Basename):

                file = os.path.join(pu.dump_directory, nsm)

                data0 = pd.read_fwf(file, na_values=['-99.000', '-9999.0', '-99999', '-9999.00', '99999'],
                                    header=None,
                                    widths=pu.width_hourly)

                data0.columns = list(pu.header_hourly.keys())

                if i == 0:
                    data = data0.copy()
                else:
                    data = data.append(data0)

            DATES = pd.date_range(DATES[0], DATES[1], freq='1H')

            columns_out = data.columns

            data_out = pd.DataFrame(columns=columns_out)

            data_out.LST_DATE = DATES.year * 10000 + DATES.month * 100 + DATES.day
            data_out.LST_TIME = DATES.hour * 100 + DATES.minute

            data_out.LONGITUDE = data.LONGITUDE.values[0]
            data_out.LATITUDE = data.LATITUDE.values[0]
            data_out.WBANNO = data.WBANNO.values[0]

            idx0, ia0, ib0 = np.intersect1d(data_out.LST_DATE * 10000 + data_out.LST_TIME,
                                            data.LST_DATE * 10000 + data.LST_TIME,
                                            return_indices=True)
            
            var_raw = np.setdiff1d(columns_out, info_col)

            for ii in var_raw:
                data_out[ii].values[ia0] = data[ii].values[ib0]

            test = info_col + VARS

            data_out = data_out[test].astype('float')

            data_out = data_out.set_index(DATES)

        except:

            data_out = None

    return data_out


def read_single_site_subhourly(ID=None,
                               DATES=['2016-01-01 00:00:00',
                                      '2016-01-31 23:59:59'],
                               VARS=['AIR_TEMPERATURE',
                                     'PRECIPITATION',
                                     'SOLAR_RADIATION',
                                     'RELATIVE_HUMIDITY'],
                               info_col = ['WBANNO', 'LONGITUDE', 'LATITUDE']):
    '''
    
    This function is to read **subhourly** data at single site. The subhourly time series is an interval of 5-min.
    
    To check available variables in subhourly data, please:
    
    >>> import pyUSCRN as pu
    >>> pu.list_available_variables(scale='subhourly')
    
    :param ID: single station ID to be read.
    :type ID: list
    
    :param DATES: date range, the first element is onset, last one is endding. It defaults to ['2016-01-01 00:00:00', '2016-01-31 23:59:59']
    :type DATES: list, lengh = 2
    
    :param VARS: variable list to be extracted, default to ['AIR_TEMPERATURE', 'PRECIPITATION', 'SOLAR_RADIATION', 'RELATIVE_HUMIDITY'].
    :type VARS: str, list
    
    :return: selected variables for given site, it has an index of datetime-style.
    :rtype: pandas.DataFrame
    
    :Example:
    
    If we want to read data at Barrow, where ID is 'AK_Utqiagvik_formerly_Barrow_4_ENE'.
    
    >>> import pyUSCRN as pu
    >>> data = pu.read_single_site_subhourly(ID=['AK_Utqiagvik_formerly_Barrow_4_ENE'])
    
    Then you can quick plot the processed data. For example, to show air temperature:
    
    >>> data.AIR_TEMPERATURE.plot()    
       
    '''

    if ID is not None:
    
#        ID = list([ID])

        assert type(ID).__name__ in ('list')
        assert len(ID) == 1

        VARS = list(VARS)

        year_range_limit = np.arange(int(DATES[0][0:4]), int(DATES[1][0:4]) + 2)
        #        if int(DATES[1][0:4])==int(DATES[0][0:4]):
        #            year_range_limit = np.arange(int(DATES[0][0:4]),int(DATES[1][0:4])+1)
        #        else:
        #            year_range_limit = np.arange(int(DATES[0][0:4]),int(DATES[1][0:4]))

        pu.check_site_files(ID=ID, scale='Subhourly')

        lst = pu.file_dict[pu.file_dict.ID.isin(ID) &
                           pu.file_dict.Source.isin(['Subhourly']) &
                           pu.file_dict.YEAR.isin(list(year_range_limit))]

        try:
            assert lst.shape[0] > 0

            lst.index = np.arange(lst.shape[0])

            for i, nsm in enumerate(lst.Basename):

                file = os.path.join(pu.dump_directory, nsm)

                data0 = pd.read_fwf(file, na_values=['-99.000', '-9999.0', '-99999', '-9999.00', '99999'],
                                    header=None,
                                    widths=pu.width_subhourly)

                data0.columns = list(pu.header_subhourly.keys())

                if i == 0:
                    data = data0.copy()
                else:
                    data = data.append(data0)

            DATES = pd.date_range(DATES[0], DATES[1], freq='5min')
            #
            columns_out = data.columns
            #
            data_out = pd.DataFrame(columns=columns_out)
            #
            data_out.LST_DATE = DATES.year * 10000 + DATES.month * 100 + DATES.day
            data_out.LST_TIME = DATES.hour * 100 + DATES.minute
            #
            data_out.LONGITUDE = data.LONGITUDE.values[0]
            data_out.LATITUDE = data.LATITUDE.values[0]
            data_out.WBANNO = data.WBANNO.values[0]
            #
            idx0, ia0, ib0 = np.intersect1d(data_out.LST_DATE * 10000 + data_out.LST_TIME,
                                            data.LST_DATE * 10000 + data.LST_TIME,
                                            return_indices=True)
            #
            
            #
            var_raw = np.setdiff1d(columns_out, info_col)
            #
            for ii in var_raw:
                #
                data_out[ii].values[ia0] = data[ii].values[ib0]
            #
            test = info_col + VARS
            #
            data_out = data_out[test].astype('float')
            #
            data_out = data_out.set_index(DATES)

        except:

            data_out = None

    return data_out


# ==============================================================================

def filter_stations(name=None,
                    lat=None,
                    lon=None,
                    elv=None,
                    cntry=['US'],
                    states=None,
                    CONUS=False,
                    network=['USCRN'],
                    operation=['Operational'],
                    shp={'filename': None, 'attrname': None, 'attr': None}):
    '''

    This fun is to select stations fron 'stations.tsv' file
    including 15 columns: 'WBAN', 'COUNTRY', 'STATE', 'LOCATION', 'VECTOR', 'NAME', 'LATITUDE','LONGITUDE', 'ELEVATION', 'STATUS', 'COMMISSIONING', 'CLOSING','OPERATION', 'PAIRING', 'NETWORK'

    :param name: Location name, defaults to [None]
    :type name: str, list
    
    :param lat: latitude, degrees_north [length = 2], defaults to [None]
    :type lat: float, list

    :param lon: longitude, degrees_east [length = 2], defaults to [None]
    :type lon: float, list

    :param elv: elevation, meters [length = 2], defaults to [None]
    :type elv: float, list

    :param cntry: country name, defaults to ['US']
    :type cntry: str, list

    :param states: state name abbreviation, defaults to [None]
    :type states: str, list

    :param CONUS: option of selecting CONUS, defaults to [False]
    :type CONUS: bool

    :param network: network name, defaults to ['USCRN']
    :type network: str, list

    :param operation: operation status, defaults to ['Operational']
    :type operation: str, list

    :param shp: shape file to be used for selecting stations, defaults to {'filename': None, 'attrname': None, 'attr': None}
    :type shp: dict

    >>> shp ={'filename':shpfile,'attrname':'dump', 'attr':['test']}

    :return: selected stations, including all 15 columns.
    :rtype: pandas.DataFrame

    :Example:

    If we want to select stations by a shape file within the CONUS, we can use:

    >>> import pyUSCRN as pu
    >>> shpfile = 'test.shp'
    >>> pu.filter_stations(cntry=['US'], shp={'filename':shpfile}, CONUS=True)

    Then, we can quick show the selected stations on map. The shapefile could be added by using 'overlap'.

    >>> pu.plot_map_selected_stations(overlap={'filename':shpfile})

    '''
    # Check inputs:
    assert (((np.size(lat) == 2) & (type(lat).__name__ in ('list', 'tuple', 'ndarray'))) | (lat is None))
    assert (((np.size(lon) == 2) & (type(lon).__name__ in ('list', 'tuple', 'ndarray'))) | (lon is None))
    assert (((np.size(elv) == 2) & (type(elv).__name__ in ('list', 'tuple', 'ndarray'))) | (elv is None))

    # Check required variables
    try:
        pu.stn_list_sel
        assert pu.stn_list_sel.shape[0] > 0
    except:
        warnings.warn('WARNING: initializing')
        pu.initialize()

    # filter of Name: -------
    if name is not None:
        find_name = np.where(pu.stn_list_sel['LOCATION'].str.contains(name, case=False) == True)[0]
    else:
        find_name = pu.stn_list_sel.index.values

    # filter of lat: --------
    if ((np.size(lat) == 2) & (type(lat).__name__ in ('list', 'tuple', 'ndarray'))):
        find_lat = np.where((pu.stn_list_sel['LATITUDE'] >= lat[0]) &
                            (pu.stn_list_sel['LATITUDE'] <= lat[1]))[0]
    else:
        find_lat = pu.stn_list_sel.index.values

    # filter of lon: --------
    if ((np.size(lon) == 2) & (type(lon).__name__ in ('list', 'tuple', 'ndarray'))):
        find_lon = np.where((pu.stn_list_sel['LONGITUDE'] >= lon[0]) &
                            (pu.stn_list_sel['LONGITUDE'] <= lon[1]))[0]
    else:
        find_lon = pu.stn_list_sel.index.values

    # filter of elv: --------
    if ((np.size(elv) == 2) & (type(elv).__name__ in ('list', 'tuple', 'ndarray'))):
        find_elv = np.where((pu.stn_list_sel['ELEVATION'] >= elv[0]) &
                            (pu.stn_list_sel['ELEVATION'] <= elv[1]))[0]
    else:
        find_elv = pu.stn_list_sel.index.values

    # filter of states: --------
    if (type(states).__name__ in ('list', 'tuple', 'ndarray')):
        find_states = np.where(pu.stn_list_sel['STATE'].isin(states))[0]
    else:
        find_states = pu.stn_list_sel.index.values

    # filter of country: --------
    if (type(cntry).__name__ in ('list', 'tuple', 'ndarray')):
        find_cntry = np.where(pu.stn_list_sel['COUNTRY'].isin(cntry))[0]
    else:
        find_cntry = pu.stn_list_sel.index.values

    # filter of CONUS: --------
    if CONUS is True:
        find_CONUS = np.where(pu.stn_list_sel['STATE'].isin(['AK', 'HI']) == False)[0]
    else:
        find_CONUS = pu.stn_list_sel.index.values

    # filter of network: --------
    if type(network).__name__ in ('list', 'tuple', 'ndarray'):
        find_network = np.where(pu.stn_list_sel['NETWORK'].isin(network) == True)[0]
    else:
        find_network = pu.stn_list_sel.index.values

    # filter of operation: --------
    if type(operation).__name__ in ('list', 'tuple', 'ndarray'):
        find_operation = np.where(pu.stn_list_sel['OPERATION'].isin(operation) == True)[0]
    else:
        find_operation = pu.stn_list_sel.index.values

    # find intersection:
    find_stn_idx = np.intersect1d(np.intersect1d(
        np.intersect1d(np.intersect1d(np.intersect1d(np.intersect1d(np.intersect1d(np.intersect1d(find_name,
                                                                                                  find_lat),
                                                                                   find_lon),
                                                                    find_elv),
                                                     find_states),
                                      find_cntry),
                       find_CONUS), find_network), find_operation)

    pu.stn_list_sel = pu.stn_list_sel.loc[find_stn_idx, :]

    if find_stn_idx.shape[0] > 0:
        pu.stn_list_sel.index = np.arange(find_stn_idx.shape[0], dtype='int')
    else:
        raise ValueError('no stations found')

    if shp['filename'] is not None:

        if ((len(shp) < 3) and (len(shp) >= 1)):
            shp['attrname'] = None
            shp['attr'] = None

        polys = pu.load_shp_from_file(shp['filename'],
                                      attrname=shp['attrname'],
                                      attr=shp['attr'])

        pts = pu.convert_to_geopandas_points(stn_list=pu.stn_list_sel)

        pu.stn_list_sel = pu.points_within_polygon(polys, pts)

    #        print(polys, pts)

    return pu.stn_list_sel
