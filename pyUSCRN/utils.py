#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, warnings,urllib
warnings.filterwarnings("ignore")

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import matplotlib.colors as mcolors
from shapely import geometry
import geopandas as gpd
import pyUSCRN as pu

# import cf_units
# import xarray as xr

pd.set_option('display.max_columns', None)

def plot_map_selected_stations(overlap=None, c= 'tab:red', 
                               mid_color_value=None,
                               cmap=plt.cm.get_cmap('seismic'),
                               ax = None,
                               **kargs):
    
    lat_min = pu.stn_list_sel['LATITUDE'].min()
    lat_max = pu.stn_list_sel['LATITUDE'].max()
    lon_min = pu.stn_list_sel['LONGITUDE'].min()
    lon_max = pu.stn_list_sel['LONGITUDE'].max()
    
    lat_min = lat_min - np.abs(lat_min) * 0.025
    lat_max = lat_max + np.abs(lat_max) * 0.025
    lon_min = lon_min - np.abs(lon_min) * 0.025
    lon_max = lon_max + np.abs(lon_max) * 0.025
    
    if lat_min<-90.:
        lat_min = -90.
    if lat_max>90:
        lat_max = 90.
    if lon_min < -180:
        lon_min = -180.
    if lon_max > 180:
        lon_max = 180.

    if ax is None:
        fig     = plt.figure()
        ax      = fig.add_subplot(111)        

    mhfd = Basemap(llcrnrlat=lat_min,urcrnrlat=lat_max,
            llcrnrlon=lon_min,urcrnrlon=lon_max, ax=ax)

    mhfd.drawcoastlines(linewidth=1.)
    mhfd.drawcountries(linewidth  =1.)
    mhfd.drawstates(linewidth = 1., color = 'k')
    mhfd.shadedrelief()
    
    if overlap is not None:
    
        if overlap['filename'] is not None:
            
            if ((len(overlap)<3) and (len(overlap)>=1)):
            
                overlap['attrname'] = None
                overlap['attr']     = None
            
            polys = pu.load_shp_from_file(overlap['filename'], 
                                  attrname=overlap['attrname'],
                                  attr= overlap['attr'])
            
            polys.plot(ax=ax, alpha=0.5, color = 'tab:blue', edgecolor='black')

    lat = np.asarray(pu.stn_list_sel['LATITUDE'])
    lon = np.asarray(pu.stn_list_sel['LONGITUDE'])
    
    if mid_color_value is not None:
        
        assert type(mid_color_value).__name__ in ['float', 'int']
        
        divnorm = mcolors.DivergingNorm(vcenter=mid_color_value)
        pts = mhfd.scatter(lon, lat, latlon=True, c = c, cmap = cmap, norm = divnorm, **kargs)
        
    else:
        
        pts = mhfd.scatter(lon, lat, latlon=True, c = c, cmap = cmap, **kargs)
    
    if np.size(c)>1:
        
        plt.colorbar(mappable = pts, orientation = 'horizontal',extend = 'both')

    ax.set_title(str(pu.stn_list_sel.shape[0])+' selected USCRN Stations')
    
    return mhfd

def points_within_polygon(polys, points):

    '''
    This function is to find points within given polygon.
    
    :param polys: polygon or polygons
    :type polys: geopandas.GeoDataFrame
    
    :param points: points
    :type points: geopandas.GeoDataFrame
    
    :return: points within given polys
    :rtype: geopandas.GeoDataFrame
        
    '''
    
    assert type(polys).__name__ == 'GeoDataFrame'
    assert type(points).__name__ == 'GeoDataFrame'

    n_polys = polys.shape[0]

    polys.index = np.arange(n_polys)

    n_polys = polys.shape[0]

    mask = np.zeros(points.shape[0])

    for i in np.arange(n_polys):
        mask += points.within(polys.loc[i, 'geometry'])

    mask_dd = points.loc[mask > 0]

    return mask_dd

def convert_to_geopandas_points(stn_list=None, 
                               coords = {'lat':'LATITUDE','lon':'LONGITUDE'}):
    
    '''
    This function is to create geopandas.GeoDataFrame from pandas.DataFrame.
    
    :param stn_list: list of sites, including at least location information, such as lat-long.
    :type stn_list: pandas.DataFrame
    
    :param coords: coordinates variables. **DO NOT CHANGE THE KEYS (strings before ":").**
    :type coords: dict
    
    :return: points
    :rtype: geopandas.GeoDataFrame
        
    '''
    

    assert stn_list is not None
    assert type(stn_list).__name__ == 'DataFrame'
    assert np.sum(stn_list.columns.isin(coords.values()))==2
    
    geometry1 = [geometry.Point(xy) for xy in zip(stn_list[coords['lon']], 
                 stn_list[coords['lat']])]
    
    gdf = gpd.GeoDataFrame(stn_list, geometry = geometry1)    
    
    return gdf

def load_shp_from_file(shpfile, attrname=None, attr=None):

    '''
    This function is to load geopandas.GeoDataFrame from shape file.
    It will merge polygons together.
    It can select attributions.
    It convert all supported projection to 'EPSG:4326'.
    If attrname or attr is None, it will read entire shape file and merge them together.
    
    :param shpfile: shape file name.
    :type shpfile: str
    
    :param attrname: attribution name
    :type attrname: str
    
    :param attr: attribution values
    :type attr: str
    
    :return: polys
    :rtype: geopandas.GeoDataFrame
    
    :Example:
    
    >>> import pyUSCRN as pu
    >>> shpfile = 'Miss_RiverBasin/Miss_RiverBasin.shp'
    >>> shdf = pu.load_shp_from_file(shpfile)
    >>> shdf.plot()
            
    '''    

    try:
    
        shdf = gpd.read_file(shpfile).to_crs({'init': 'epsg:4326'})
        
    except:
        
        raise ValueError('Failed to read shape file')
    
    if ((attrname is None) or (attr is None)):
                
        shdf['dump']='all'
        shdf = shdf.dissolve(by='dump')
        
        attrname0 = [shdf.columns[-1]]
        
        attr0 = list(shdf[attrname0].values[0])
                
        idx = np.where(shdf[attrname0].values in attr0)[0]
        
        shdf = shdf.iloc[idx,]

    if ((attrname is not None) and (attr is not None)):
        
        assert len(attrname) == 1
        assert type(attrname).__name__ in ['list']
        assert type(attr).__name__ in ['list']
        assert type(shdf).__name__ in ['GeoDataFrame']   
        
        idx = np.where(shdf[attrname].isin(attr))[0]

        shdf = shdf.iloc[idx,]    
            
        shdf['dump']='all'
        shdf = shdf.dissolve(by='dump')

    return shdf

def check_site_files(ID=None, force = False, quiet = True, scale = 'Daily'):

    '''
    This function is to check raw data files for a list of station IDs.

    :param ID: a list of station IDs
    :type ID: list
    
    :param force: forcely check and download files from FTP, default to False.
    :type force: bool
        
    :param quiet: block print function, default to True.
    :type quiet: bool
    
    :param scale: time scale, could be 'daily', 'monthly', 'hourly', or 'subhourly', default to 'Daily'.
    :type scale: str
            
    ''' 
        
    assert scale.lower() in ['daily','monthly','hourly','subhourly']
    
    scale = scale.capitalize()
    
    if ID is not None:
        
        lst = pu.file_dict[pu.file_dict.ID.isin(ID) & pu.file_dict.Source.isin([scale])]
                        
        assert lst.shape[0] > 0
                
        lst.index = np.arange(lst.shape[0])
        
        for i, isd in enumerate(lst.FTP):
            
            file = lst.Basename[i]
            
            exist = os.path.isfile(os.path.join(pu.dump_directory, file))
                        
            if (exist == False) | (force == True):
        
                try:
                    
                    urllib.request.urlretrieve(lst.FTP[i], 
                                               filename=os.path.join(pu.dump_directory, file))
                except:
                    
                    raise ConnectionError('can not obtain file from FTP')
            
            if quiet == False:
                
                print('{:5} | {:40} | {:10}'.format(i+1, file, 'OK'))