# Import Modules

import urllib, os, warnings

warnings.filterwarnings("ignore")

from urllib.parse import urlparse
from ftplib import FTP
import pandas as pd
import numpy as np

import pyUSCRN as pu

from .iotools import *
from .utils import *

pd.set_option('display.max_columns', None)

ftp_link = 'ftp://ftp.ncdc.noaa.gov/pub/data/uscrn/products/'

stn_list_filename = 'stations.tsv'
ftp_file_list = 'ftp_list.txt'

pkg_directory = os.path.dirname(os.path.realpath(__file__))

dump_directory = os.path.join(pkg_directory, 'dump/')

if os.path.isdir(dump_directory) == False:
    os.mkdir(dump_directory)

time_scales = {'Monthly': 'monthly01',
               'Daily': 'daily01',
               'Hourly': 'hourly02',
               'Subhourly': 'subhourly01',
               'Soil': 'soil01'}

#================MONTHLY=====================

header_monthly = {'WBANNO': '1',
                  'LST_YRMO': '1',
                  'CRX_VN_MONTHLY': '1',
                  'PRECISE_LONGITUDE': '1',
                  'PRECISE_LATITUDE': '1',
                  'T_MONTHLY_MAX': 'Celsius',
                  'T_MONTHLY_MIN': 'Celsius',
                  'T_MONTHLY_MEAN': 'Celsius',
                  'T_MONTHLY_AVG': 'Celsius',
                  'P_MONTHLY_CALC': 'mm per month',
                  'SOLRAD_MONTHLY_AVG': 'Watt m^-2', #NOTE: Raw unit (MJ m-2) has been converted.
                  'SUR_TEMP_MONTHLY_TYPE': '1',
                  'SUR_TEMP_MONTHLY_MAX': 'Celsius',
                  'SUR_TEMP_MONTHLY_MIN': 'Celsius',
                  'SUR_TEMP_MONTHLY_AVG': 'Celsius'}

width_monthly = [5, 6 + 1, 6 + 1, 9 + 1, 9 + 1, 
                 7 + 1, 7 + 1, 7 + 1, 7 + 1, 7 + 1, 
                 7 + 1, 1 + 1, 7 + 1, 7 + 1, 7 + 1]

#================HOURLY=====================

header_hourly = {'WBANNO': '1',
                 'UTC_DATE': '1',
                 'UTC_TIME': '1',
                 'LST_DATE': '1',
                 'LST_TIME': '1',
                 'CRX_VN': '1',
                 'LONGITUDE': '1',
                 'LATITUDE': '1',
                 'T_CALC': 'Celsius',
                 'T_HR_AVG': 'Celsius',
                 'T_MAX': 'Celsius',
                 'T_MIN': 'Celsius',
                 'P_CALC': 'mm per hour',
                 'SOLARAD': 'Watt m^-2',
                 'SOLARAD_FLAG': '1',
                 'SOLARAD_MAX': 'Watt m^-2',
                 'SOLARAD_MAX_FLAG': '1',
                 'SOLARAD_MIN': 'Watt m^-2',
                 'SOLARAD_MIN_FLAG': '1',
                 'SUR_TEMP_TYPE': '1',
                 'SUR_TEMP': 'Celsius',
                 'SUR_TEMP_FLAG': '1',
                 'SUR_TEMP_MAX': 'Celsius',
                 'SUR_TEMP_MAX_FLAG': '1',
                 'SUR_TEMP_MIN': 'Celsius',
                 'SUR_TEMP_MIN_FLAG': '1',
                 'RH_HR_AVG': '%',
                 'RH_HR_AVG_FLAG': '1',
                 'SOIL_MOISTURE_5': 'm^3 m^-3',
                 'SOIL_MOISTURE_10': 'm^3 m^-3',
                 'SOIL_MOISTURE_20': 'm^3 m^-3',
                 'SOIL_MOISTURE_50': 'm^3 m^-3',
                 'SOIL_MOISTURE_100': 'm^3 m^-3',
                 'SOIL_TEMP_5': 'Celsius',
                 'SOIL_TEMP_10': 'Celsius',
                 'SOIL_TEMP_20': 'Celsius',
                 'SOIL_TEMP_50': 'Celsius',
                 'SOIL_TEMP_100': 'Celsius'}

width_hourly = [5  , 8+1, 4+1, 8+1, 4+1,
                6+1, 7+1, 7+1, 7+1, 7+1,
                7+1, 7+1, 7+1, 6+1, 1+1,
                6+1, 1+1, 6+1, 1+1, 1+1,
                7+1, 1+1, 7+1, 1+1, 7+1,
                1+1, 5+1, 1+1, 7+1, 7+1,
                7+1, 7+1, 7+1, 7+1, 7+1,
                7+1, 7+1, 7+1]

#================SUBHOURLY=====================

header_subhourly = {'WBANNO': '1',
                    'UTC_DATE': '1',
                    'UTC_TIME': '1',
                    'LST_DATE': '1',
                    'LST_TIME': '1',
                    'CRX_VN': '1',
                    'LONGITUDE': '1',
                    'LATITUDE': '1',
                    'AIR_TEMPERATURE': 'Celsius',
                    'PRECIPITATION': 'mm',
                    'SOLAR_RADIATION': 'Watt m^-2',
                    'SR_FLAG': '1',
                    'SURFACE_TEMPERATURE': 'Celsius',
                    'ST_TYPE': '1',
                    'ST_FLAG': '1',
                    'RELATIVE_HUMIDITY': '%',
                    'RH_FLAG': '1',
                    'SOIL_MOISTURE_5': 'm^3 m^-3',
                    'SOIL_TEMPERATURE_5': 'Celsius',
                    'WETNESS': 'Ohms',
                    'WET_FLAG': '1',
                    'WIND_1_5': 'm s-1',
                    'WIND_FLAG': '1',
                    }

width_subhourly = [5  , 8+1, 4+1, 8+1, 4+1,
                   6+1, 7+1, 7+1, 7+1, 7+1,
                   6+1, 1+1, 7+1, 1+1, 1+1,
                   5+1, 1+1, 7+1, 7+1, 5+1,
                   1+1, 6+1, 1+1]

#================DAILY=====================

header_daily = {'WBANNO': '1',
                'LST_DATE': '1',
                'CRX_VN': '1',
                'LONGITUDE': '1',
                'LATITUDE': '1',
                'T_DAILY_MAX': 'Celsius',
                'T_DAILY_MIN': 'Celsius',
                'T_DAILY_MEAN': 'Celsius',
                'T_DAILY_AVG': 'Celsius',
                'P_DAILY_CALC': 'mm per day',
                'SOLARAD_DAILY': 'Watt m^-2', #NOTE: Raw unit (MJ m-2) has been converted.
                'SUR_TEMP_DAILY_TYPE': '1',
                'SUR_TEMP_DAILY_MAX': 'Celsius',
                'SUR_TEMP_DAILY_MIN': 'Celsius',
                'SUR_TEMP_DAILY_AVG': 'Celsius',
                'RH_DAILY_MAX': '%',
                'RH_DAILY_MIN': '%',
                'RH_DAILY_AVG': '%',
                'SOIL_MOISTURE_5_DAILY': 'm^3 m^-3',
                'SOIL_MOISTURE_10_DAILY': 'm^3 m^-3',
                'SOIL_MOISTURE_20_DAILY': 'm^3 m^-3',
                'SOIL_MOISTURE_50_DAILY': 'm^3 m^-3',
                'SOIL_MOISTURE_100_DAILY': 'm^3 m^-3',
                'SOIL_TEMP_5_DAILY': 'Celsius',
                'SOIL_TEMP_10_DAILY': 'Celsius',
                'SOIL_TEMP_20_DAILY': 'Celsius',
                'SOIL_TEMP_50_DAILY': 'Celsius',
                'SOIL_TEMP_100_DAILY': 'Celsius'}

width_daily = [5, 8 + 1, 6 + 1, 7 + 1, 7 + 1, 
               7 + 1, 7 + 1, 7 + 1, 7 + 1, 7 + 1, 
               8 + 1, 1 + 1, 7 + 1, 7 + 1, 7 + 1,
               7 + 1, 7 + 1, 7 + 1,7 + 1, 7 + 1, 
               7 + 1, 7 + 1, 7 + 1,7 + 1, 7 + 1, 
               7 + 1, 7 + 1, 7 + 1]

def intro():
    ''' 
    It will print out some basic information of this package
    
    :Example:
    
    >>> import pyUSCRN as pu
    >>> pu.intro()
    '''

    print("################################################################################")
    print("Name: pyUSCRN")
    print("A python package to deal with the data from USCRN")
    print("Author: Kang Wang")
    print("University of Colorado Boulder")
    print("URL: https://gitlab.com/waccount1984/pyuscrn/")
    print("################################################################################")


def cleanup():
    '''
    It will clean up all files in [dump] folder
    of your installation directory
    
    :Example:
    
    >>> import pyUSCRN as pu
    >>> pu.cleanup()
    '''

    import shutil
    try:
        shutil.rmtree(dump_directory)
        os.mkdir(dump_directory)
    except:
        os.mkdir(dump_directory)


def create_file_inventory():

    '''
    This function is to create a dict variable storing file list, year, time scale, etc.
    This information could be useful to select and load part of files.
    
	:Example:
	
	>>> import pyUSCRN as pu
	>>> pu.create_file_inventory()	
	    
    '''

    file_dict = pd.DataFrame()

    for iis in pu.time_scales:

        filename = os.path.join(pu.pkg_directory, iis + '.txt')
        filelist = pd.read_csv(filename, header=None)

        filelist = pd.DataFrame(filelist)
        filelist.columns = ['FTP']

        filelist['Source'] = iis
        filelist['YEAR'] = int(9999)
        filelist['Basename'] = filelist['FTP'].apply(lambda x: os.path.basename(x))
        if iis == 'Soil':
            filelist['ID'] = filelist['Basename'].apply(lambda x: x[7:-4])
        if iis == 'Monthly':
            filelist['ID'] = filelist['Basename'].apply(lambda x: x[9:-4])
        if iis == 'Daily':
            filelist['ID'] = filelist['Basename'].apply(lambda x: x[14:-4])
            filelist['YEAR'] = filelist['Basename'].apply(lambda x: int(x[9:13]))
        if iis == 'Hourly':
            filelist['ID'] = filelist['Basename'].apply(lambda x: x[14:-4])
            filelist['YEAR'] = filelist['Basename'].apply(lambda x: int(x[9:13]))
        if iis == 'Subhourly':
            filelist['ID'] = filelist['Basename'].apply(lambda x: x[17:-4])
            filelist['YEAR'] = filelist['Basename'].apply(lambda x: int(x[12:16]))

        file_dict = file_dict.append(filelist)

    file_dict.index = np.arange(file_dict.shape[0])

    pu.file_dict = file_dict

    return pu.file_dict

def read_station_information():

    '''
    This fun is to read station information from 'stations.tsv' file
    including 15 columns:
    'WBAN', 'COUNTRY', 'STATE', 'LOCATION', 'VECTOR', 'NAME', 'LATITUDE',
    'LONGITUDE', 'ELEVATION', 'STATUS', 'COMMISSIONING', 'CLOSING',
    'OPERATION', 'PAIRING', 'NETWORK'
    
    :Example:
    
    >>> import pyUSCRN as pu
    >>> pu.read_station_information()	
	
    '''
    pu.stn_list = pd.read_csv(pu.pkg_directory + '/stations.tsv', sep='\t')
    test_sites = np.where(pu.stn_list['STATUS'].str.contains('Test') == False)[0]
    pu.stn_list = pu.stn_list.iloc[test_sites, :]
    n_sites = pu.stn_list.shape[0]
    pu.stn_list.index = np.arange(n_sites)
    
    pu.stn_list['ELEVATION'] = pu.stn_list['ELEVATION'].astype('float') * 0.3048

    pu.stn_list['ID'] = pu.stn_list.STATE + '_' + pu.stn_list.LOCATION.apply(
        lambda x: x.replace(' ', '_')) + '_' + pu.stn_list.VECTOR.apply(lambda x: x.replace(' ', '_'))

    pu.stn_list_sel = pu.stn_list.copy()

    return pu.stn_list


def refresh_ftp_file_list(force=True):

    '''
	This function is to update file list from the FTP server, in order to keep the local 
	information is up-to-date.
	
	:param force: mandatory option, default to [True].
	:type force: bool
	
	:Example:
	
	>>> import pyUSCRN as pu
	>>> pu.refresh_ftp_file_list()	
	
	'''

    if force is True:

        ftp_id = FTP(urlparse(pu.ftp_link).netloc)
        ftp_id.login("anonymous")

        for iis in pu.time_scales:

            if (iis == 'Monthly'):

                ftp_id.cwd("/pub/data/uscrn/products/" + pu.time_scales[iis])

                fli = ftp_id.nlst('CRN*')

                fid = open(os.path.join(pu.pkg_directory, iis + '.txt'), 'wt')
                for ipp in fli:
                    fid.write(os.path.join(pu.ftp_link, pu.time_scales[iis], ipp) + '\n')
                fid.close()

            if (iis == 'Daily') | (iis == 'Hourly') | (iis == 'Subhourly'):

                ftp_id.cwd("/pub/data/uscrn/products/" + pu.time_scales[iis])

                fli = ftp_id.nlst('2*')

                fid = open(os.path.join(pu.pkg_directory, iis + '.txt'), 'wt')
                for ipp in fli:
                    fid.write(os.path.join(pu.ftp_link, pu.time_scales[iis], ipp) + '\n')
                fid.close()

            if (iis == 'Soil'):

                ftp_id.cwd("/pub/data/uscrn/products/" + pu.time_scales[iis])

                fli = ftp_id.nlst('SOIL01*')

                fid = open(os.path.join(pu.pkg_directory, iis + '.txt'), 'wt')
                for ipp in fli:
                    fid.write(os.path.join(pu.ftp_link, pu.time_scales[iis], ipp) + '\n')
                fid.close()


def refresh_stations_list(force=True):
    '''
    This function is to load station list from the inv file [stations.tsv].
    
    :param force: the option, default to [True]
    :type force: bool
    
    :Example:
	
	>>> import pyUSCRN as pu
	>>> pu.refresh_stations_list()
	
    '''

    file = os.path.join(pu.ftp_link, pu.stn_list_filename)

    # STEP1 - download if force option is True.
    if (force is True):
        try:
            urllib.request.urlretrieve(file,
                                       filename=os.path.join(pu.pkg_directory, pu.stn_list_filename))
        except:
            raise IOError('Remote File [' + file + '] not existed...')


def initialize(force=False):
    '''
    initialize the basic information from FTP
    
    :param force: the option, default to [False]. Because if it is true, every import will cost more time.     
    :type force: bool
    
    STEP1 - Check the required files
    
    STEP2 - refresh files from FTP
    
    In other words, it will forcedly run
    
    >>> pu.refresh_stations_list()
    >>> pu.refresh_ftp_file_list()
    >>> pu.read_station_information()
    >>> pu.create_file_inventory()
    
    :param force: the option, default to [True]
    :type force: bool
    
    :Example:
    
    >>> import pyUSCRN as pu
    >>> pu.initialize()
    
    '''

    # STEP1 - Check the required files

    ex_01 = os.path.isfile(os.path.join(pu.pkg_directory, 'Monthly.txt'))
    ex_02 = os.path.isfile(os.path.join(pu.pkg_directory, 'Daily.txt'))
    ex_03 = os.path.isfile(os.path.join(pu.pkg_directory, 'Hourly.txt'))
    ex_04 = os.path.isfile(os.path.join(pu.pkg_directory, 'Subhourly.txt'))
    ex_05 = os.path.isfile(os.path.join(pu.pkg_directory, 'Soil.txt'))
    ex_06 = os.path.isfile(os.path.join(pu.pkg_directory, pu.stn_list_filename))

    # STEP2 - if there is one or more missing files, or force equal true
    #        it will refresh station list file and ftp data files from
    #        the FTP server.

    if (ex_01 * ex_02 * ex_03 * ex_04 * ex_05 * ex_06 == 0) | (force is True):
        refresh_stations_list()
        refresh_ftp_file_list()

    read_station_information()
    create_file_inventory()


initialize()
