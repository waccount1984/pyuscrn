# pyUSCRN: Data component for USCRN data

pyUSCRN is developed for dealing with data from the U.S. Climate Reference Network (USCRN) Database.

Background
------------

The U.S. Climate Reference Network (USCRN) is a systematic and sustained network of climate monitoring stations with sites across the conterminous U.S., Alaska, and Hawaii. These stations use high-quality instruments to measure temperature, precipitation, wind speed, soil conditions, and more. Information is available on what is measured and the USCRN station instruments.

The vision of the USCRN program is to provide a continuous series of climate observations for monitoring trends in the nation's climate and supporting climate-impact research.

![](_images/crn_map.png)

More Information about USCRN can be found [here.](https://www.ncdc.noaa.gov/crn/)

Installing pyUSCRN
------------

pyUSCRN should work on Windows, Mac OSX and Linux, however has only been tested in Mac OSX and Linux environments. 
The code was developed using the Python 3.7 Anaconda Python Distribution from Continuum Analytics. 
The Anaconda Python Distribution can be downloaded here: https://store.continuum.io/cshop/anaconda/

Other methods may work (i.e. Macports, built in Python, etc), but has not been tested. Any issues can be reported through contact information below.

__Required Python Packages/Libraries:__

 - numpy
 - netCDF4
 - shapely
 - pandas
 - geopandas
 - cf_units
 - xarray 
 - requests
 - matplotlib

 If these packages are not installed on your system, they will need to be installed. 
Using conda, this comand can be used:

> conda create -n py_USCRN_prj

> conda activate py_USCRN_prj

> conda install -c conda-forge cf_units pycrs numpy geopandas=0.5.1 gdal=2.4.1 basemap=1.2.1 xarray=0.12.3 shapely=1.6.4

Once the packages are installed, you're ready to install pyUSCRN. Using git:

> git clone https://gitlab.com/waccount1984/pyuscrn.git

Once the package has been downloaded to your computer you can setup using this command:

> python setup.py install

After installing the package, you can test by using this command:

> python -c "import pyUSCRN as pu; pu.intro()"

You are now ready to go!

Contact
------------
Any questions, comments, bug reports, feature requests, and other inquiries should be sent to kang.wang@colorado.edu
