from setuptools import setup, find_packages

setup(
    name='pyUSCRN',
    version='1.0',
    author='Kang Wang',
    author_email='Kang.Wang@colorado.edu',
    packages=find_packages(),
    url='https://github.com/wk1984/pyUSCRN/',
    description='Package for Dealing with Data from USCRN',
    long_description=open('README.md').read(),
    license='MIT',
    keywords = "USCRN data toolbox",
    scripts=['pyUSCRN/iotools.py','pyUSCRN/utils.py'],
    data_files = [("", ["LICENSE"])],
    install_requires=[
          'numpy',
          'netCDF4',
          'shapely',
          'pandas',
          'geopandas',
          'requests',
          'matplotlib',
          'cf_units',
          'xarray',
          'basemap',
    ],
)
